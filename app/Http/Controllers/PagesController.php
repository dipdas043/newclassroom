<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Framework\Sessions;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function userlist(){
        if(!Auth::check()){
            return 'To see the student list you have to log in first!!';
        }
        elseif(Auth::user()->role == 1){
            $users = DB::table('users')->where('role', '2')->get();
            return view('users.userlist')->with('users',$users);
        }
        else{
            return 'Only teacher can see the list of all students!!';
        }
    }
    public function destroy($id)
    {
        if(!Auth::check()){
            echo 'To see the student list you have to log in first!!';
        }
        elseif(Auth::user()->role == 1){
        DB::delete('delete from users where id = ?',[$id]);
        echo 'Student profile has been deleted successfully!!';
    }
    else{
        echo 'Only teacher can see the list of all students!!';
    } 
    }
}
