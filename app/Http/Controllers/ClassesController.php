<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes;
use Auth;
use Framework\Sessions;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return 'To see the class list you have to log in first!!';
        }
        $classes = Classes::orderBy('id','desc')->get();
        return view('classes.classlist')->with('classes',$classes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        elseif(Auth::user()->role == 1){
            return view('classes.create');
        }
        else{
            return 'Only teacher can create a class!!';
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:10|max:100',
            'date' => 'required',
            'time' => 'required'
        ]);
        $classes = new Classes;

        $classes->title = $request->title;
        $classes->date = $request->date;
        $classes->time = $request->time;

        $classes->save();

        //passes massege after save
        $request->session()->flash('success', 'The class was successfully posted!');

        //redirect to another page
        return redirect()->route('classes.show', $classes->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $classes = Classes::find($id);
        return view('classes.show')->withClasses($classes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        elseif(Auth::user()->role == 1){
        $classes = Classes::find($id);
        return view('classes.edit')->withClasses($classes);
    }
    else{
        return 'Only a teacher can edit a class!!';
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classes = Classes::find($id);
        $validatedData = $request->validate([
            'title' => 'required|min:10|max:100',
            'date' => 'required',
            'time' => 'required'
        ]);

        $classes->title = $request->title;
        $classes->date = $request->date;
        $classes->time = $request->time;

        $classes->update();

        //passes massege after save
        $request->session()->flash('success', 'The post was successfully updated!');

        //redirect to another page
        return redirect()->route('classes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        elseif(Auth::user()->role == 1){
        DB::delete('delete from classes where id = ?',[$id]);
        $request->session()->flash('success', 'The post was successfully deleted!');
        return redirect()->route('classes.index');  
    }
    else{
        return 'Only a teacher can delete a class!!';
    }
    }
}
