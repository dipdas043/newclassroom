@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <center><strong><div class="card-header">{{ Auth::user()->name }}{{ __("'s PROFILE") }}</div></strong></center>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>
                        <a class="btn btn-primary" href="/userlist" role="submit">Student list</a>
                        <a class="btn btn-primary" href="/classlist" role="submit">class list</a>
                        <a class="btn btn-primary" href="/create" role="submit">create class</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
