@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               <center><strong> <div class="card-header">{{ __('Create new Class') }}</div></strong></center>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
<div class="row">
  <div class="col-md-6 offset-md-3">
    <div class="card">
      <div class="card-header">
        Create a class
      </div>
      <div class="card-body">
      <form method="POST" action="{{route('classes.store')}}">
        @csrf  
        <div class="form-group">
            <label for="title">Class Title</label>
            <input type="text" class="form-control" id="title" name="title">
            @error('title')
               <span class="text-danger">{{$message}}</span> 
            @enderror
          </div>
          <div class="form-group">
            <label for="date">Date</label>
            <input type="date" class="form-control" id="date" name="date">
            @error('date')
            <span class="text-danger">{{$message}}</span> 
         @enderror
          </div>
          <div class="form-group">
            <label for="time">Time</label>
            <input type="time" class="form-control" id="time" name="time">
            @error('time')
            <span class="text-danger">{{$message}}</span> 
         @enderror
          </div>
          <center><button type="submit" class="btn btn-primary">Submit</button></center>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection