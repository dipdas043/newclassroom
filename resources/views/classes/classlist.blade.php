@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               <center><strong> <div class="card-header">{{ __('Class List') }}</div></strong></center>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <table class="table table-hover table-dark">
                  <thead>
                    <tr>
                      <th scope="col">Title</th>
                      <th scope="col">Date</th>
                      <th scope="col">time</th>
                      <th scope="col">Edit</th>
                      <th scope="col">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($classes) > 0)
                    @foreach ($classes as $classes)
                    <tr>
                      <td scope="row">{{$classes->title}}</td>
                      <td>{{$classes->date}}</td>
                      <td>{{$classes->time}}</td>
                    <td><a class="btn btn-warning" href="{{ route('classes.edit',$classes->id) }}" role="button">Edit</a></td>
                    <td><a class="btn btn-danger" href = 'delete/{{ $classes->id }}'>Delete</a></td></td>
                    </tr>
                    @endforeach
                    @else
                       <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                          {{ __('No Class created yet') }}
                       </h2> 
                    @endif 
                  </tbody>
                </table> 
</div>
</div>
</div>
</div>
</div>
@endsection