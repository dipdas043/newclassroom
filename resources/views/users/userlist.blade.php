@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               <center><strong> <div class="card-header">{{ __('Class List') }}</div></strong></center>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <table class="table table-hover table-dark">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($users) > 0)
                    @foreach ($users as $users)
                    <tr>
                      <td scope="row">{{$users->name}}</td>
                      <td>{{$users->email}}</td>
                      <td><a class="btn btn-danger" href = 'udelete/{{ $users->id }}'>Delete</a></td></td>
                    </tr>
                    @endforeach
                    @else
                       <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                          {{ __('No Students yet') }}
                       </h2> 
                    @endif 
                  </tbody>
                </table> 
</div>
</div>
</div>
</div>
</div>
@endsection