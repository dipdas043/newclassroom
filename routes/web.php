<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('classes','ClassesController');
Route::resource('welcome','PagesController');
Route::get('/','PagesController@index');
Route::get('udelete/{id}','PagesController@destroy');
//Route::get('/destroy/{id}','PagesController@destroy');
Auth::routes();
Route::any('/admin', 'AdminController@index')->name('admin')->middleware('admin');
Route::any('/user', 'UserController@index')->name('user')->middleware('user');
Route::get('/create','ClassesController@create')->name('create');
Route::get('/classlist','ClassesController@index')->name('classlist');
Route::get('/userlist', 'PagesController@userlist')->name('userlist');
Route::any('edit/{id}','ClassesController@edit');
Route::get('delete/{id}','ClassesController@destroy');
//Route::any('destroy/{id}','ClassesController@destroy');
//Route::any('/home', 'HomeController@index')->name('home');
//Route::post('/admin', 'AdminController@index')->name('admin')->middleware('admin');
//Route::get('/user', 'UserController@index')->name('user')->middleware('user');
//Route::get('/home', 'HomeController@index')->name('home');
